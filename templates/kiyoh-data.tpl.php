<?php
/**
 * @file
 * Template file of Kiyoh module.
 *
 * - $average_score_last_month: Average result of last month.
 * - $reviews_last_month: Reviews of last month.
 * - $reviews_total: Total reviews.
 *
 * @var $average_score_last_month
 * @var $reviews_last_month
 * @var $reviews_total
 */
?>

<div class="block-kiyoh">
  <ul>
    <?php if(isset($average_score_last_month)): ?>
      <li>
        <?php print t('Average Score Last Month: %value', array('%value' => $average_score_last_month)); ?>
      </li>
    <?php endif; ?>
    <?php if(isset($reviews_last_month)): ?>
      <li>
        <?php print t('Review Last Month: %value', array('%value' => $reviews_last_month)); ?>
      </li>
    <?php endif; ?>
    <?php if(isset($reviews_total)): ?>
      <li>
        <?php print t('Reviews Total: %value', array('%value' => $reviews_total)); ?>
      </li>
    <?php endif; ?>
  </ul>
</div>
