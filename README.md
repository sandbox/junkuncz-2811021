## Introduction

This module provides a bridge between Drupal and Kiyoh XML API.
(https://www.kiyoh.nl/)  

## Requirements

* Core Block module

## Installation

On traditional drupal way  
(https://www.drupal.org/documentation/install/modules-themes/modules-7)

## Configuration

With given role on the admin/structure/kiyoh page  
Here you can set up your Kiyoh ID.

## Maintainers

@junkuncz https://www.drupal.org/u/junkuncz
